package de.griffel.confluence.plugins.plantuml.config;

import net.sourceforge.plantuml.FileFormat;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * Java Bean holding the {@link PlantUmlConfiguration} properties.
 */
public final class PlantUmlConfigurationBean implements Serializable, PlantUmlConfiguration {
   private static final long serialVersionUID = 1L;

   private boolean svek = true;
   private boolean fileFormatSvgDefault = false;
   private String commonHeader = StringUtils.EMPTY;
   private String commonFooter = StringUtils.EMPTY;

   public boolean isSvek() {
      return svek;
   }

   public void setSvek(boolean bool) {
      svek = bool;
   }

   public boolean isFileFormatSvgDefault() {
      return fileFormatSvgDefault;
   }

   public void setFileFormatSvgDefault(boolean fileFormatSvgDefault) {
      this.fileFormatSvgDefault = fileFormatSvgDefault;
   }

   public String getCommonHeader() {
      return commonHeader;
   }

   public void setCommonHeader(String commonHeader) {
      this.commonHeader = commonHeader;
   }

   public boolean isSetCommonHeader() {
      return !StringUtils.isEmpty(commonHeader);
   }

   public String getCommonFooter() {
      return commonFooter;
   }

   public void setCommonFooter(String commonFooter) {
      this.commonFooter = commonFooter;
   }

   public boolean isSetCommonFooter() {
      return !StringUtils.isEmpty(commonFooter);
   }

   public FileFormat getDefaultFileFormat() {
      return isFileFormatSvgDefault() ? FileFormat.SVG : FileFormat.PNG;
   }

   @Override
   public String toString() {
      final StringBuilder sb = new StringBuilder();
      sb.append("PlantUmlConfigurationBean [svek=");
      sb.append(svek);
      sb.append(", commonHeader=");
      sb.append(commonHeader);
      sb.append(", commonFooter=");
      sb.append(commonFooter);
      sb.append("]");
      return sb.toString();
   }


}
