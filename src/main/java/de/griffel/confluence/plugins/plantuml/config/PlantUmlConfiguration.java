package de.griffel.confluence.plugins.plantuml.config;

import net.sourceforge.plantuml.FileFormat;

/**
 * Configuration properties.
 */
public interface PlantUmlConfiguration {

   boolean isSvek();

   void setSvek(boolean bool);

   boolean isFileFormatSvgDefault();

   void setFileFormatSvgDefault(boolean bool);

   String getCommonHeader();

   void setCommonHeader(String commonHeader);

   boolean isSetCommonHeader();

   String getCommonFooter();

   void setCommonFooter(String commonFooter);

   boolean isSetCommonFooter();

   FileFormat getDefaultFileFormat();
}
