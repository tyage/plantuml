package de.griffel.confluence.plugins.plantuml;

import net.sourceforge.plantuml.FileFormat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DefaultDownloadResourceInfoTest {
  @Test
  public void testMimeTypeInfo() {
    assertEquals("image/png", DefaultDownloadResourceInfo.mimeType(FileFormat.PNG));
    assertEquals("image/png", DefaultDownloadResourceInfo.mimeType(FileFormat.BRAILLE_PNG));
    assertEquals("application/pdf", DefaultDownloadResourceInfo.mimeType(FileFormat.PDF));
    assertNull(DefaultDownloadResourceInfo.mimeType(FileFormat.BASE64));
  }

}