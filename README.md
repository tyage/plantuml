Confluence PlantUML Plugin
==========================

## Prerequisites
* Oracle JDK 8
* Atlassian SDK or Maven 3.x

## Development

### Building

How to build the plugin

    mvn package

### Deploying

You can deploy the plugin on a standalone Confluence server by command line

    atlas-mvn atlassian-pdk:install \
        -Datlassian.pdk.server.username=admin \
        -Datlassian.pdk.server.password=secret \
        -Datlassian.pdk.server.url=http://server:PORT \
        -Datlassian.plugin.key=puml

or by uploading the plugin from ```target/plant-uml-x.y-SNAPSHOT.jar``` via UPM (Universal Plugin Manager).

#### Confluence Docker Container

If you don't have a running Confluence server you may use this Docker container to deploy the plugin.

```
docker run -v /var/confluence.home:/var/atlassian/application-data/confluence -it --rm --name="confluence-plantuml" -d -p 8090:8090 -p 8091:8091 mgriffel/confluence-plantuml
```

Start your browser and open http://localhost:8090
